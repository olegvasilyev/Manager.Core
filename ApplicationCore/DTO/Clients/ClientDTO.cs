﻿using System;

namespace ApplicationCore.DTO.Clients
{
    public class ClientDTO
    {
        public int Id { get; set; } = 0;

        public string Country { get; set; } = "";

        public string City { get; set; } = "";

        public string Street { get; set; } = "";

        public string HomeNumber { get; set; } = null;

        public int RoleId { get; set; } = 0;

        public string RoleCode { get; set; } = "";


        public string FirstName { get; set; } = "";

        public string SurName { get; set; } = null;

        public string LastName { get; set; } = null;

        public DateTime? Birthday { get; set; } = null;

        public string BirthdayStr
        {
            get
            {
                return Birthday.HasValue ? String.Format("{0:yyyy-MM-dd}", Birthday.Value) : "";
            }
        }

        public string Passport { get; set; } = "";


        public string Name { get; set; } = "";

        public string ITN { get; set; } = "";

        public string FullName { get; set; } = "";
    }
}

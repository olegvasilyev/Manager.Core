﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.DTO.Clients
{
    public class ClientRoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

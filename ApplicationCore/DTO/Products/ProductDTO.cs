﻿namespace ApplicationCore.DTO.Products
{
    public class ProductDTO
    {
        public int Id { get; set; } = 0;

        public string Name { get; set; } = "";

        public decimal Price { get; set; } = 0;

        public int Count { get; set; } = 0;
    }
}

﻿using System;

namespace ApplicationCore.DTO.Sales
{
    public class SaleDTO
    {
        public int Id { get; set; } = 0;

        public DateTime SaleDate { get; set; }

        public string SaleDateStr
        {
            get { return SaleDate.ToString("dd.MM.yyyy HH:mm"); }
        }

        public int Count { get; set; } = 0;

        public decimal TotalCost { get; set; } = 0;

        public string ClientName { get; set; } = "";

        public string ProductName { get; set; } = "";

        public int ClientId { get; set; } = 0;

        public int ProductId { get; set; } = 0;

        public int ProductCount { get; set; } = 0;

        public decimal ProductPrice { get; set; } = 0;
    }
}

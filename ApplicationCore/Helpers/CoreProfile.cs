﻿using ApplicationCore.DTO.Clients;
using ApplicationCore.DTO.Products;
using ApplicationCore.DTO.Sales;
using AutoMapper;
using Infrastructure.Entities;

namespace Web.Helpers
{
    public class CoreProfile : Profile
    {
        public CoreProfile()
        {
            CreateMap<Client, ClientDTO>();
            CreateMap<ClientDTO, Client>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.RoleId, opt => opt.Ignore());
            CreateMap<LegalEntity, ClientDTO>().ForSourceMember(x => x.Id, opt => opt.Ignore());
            CreateMap<ClientDTO, LegalEntity>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<Person, ClientDTO>().ForSourceMember(x => x.Id, opt => opt.Ignore());
            CreateMap<ClientDTO, Person>().ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Product, ProductDTO>();
            CreateMap<ProductDTO, Product>().ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<Sale, SaleDTO>();
            CreateMap<SaleDTO, Sale>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x=>x.ClientId, opt=>opt.Ignore())
                .ForMember(x => x.ProductId, opt => opt.Ignore());
        }
    }
}

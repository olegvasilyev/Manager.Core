﻿using ApplicationCore.DTO.Clients;
using ApplicationCore.Models;
using ApplicationCore.Models.Clients;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IClientsService : IDisposable
    {
        Task<ClientsModel> GetClientsAsync(DataTableModel parameters);
        Task<ClientResult> GetClientAsync(int id);
        Task<List<ClientRoleDTO>> GetClientRoles();
        Task<ResultBase<bool>> EditClientAsync(ClientDTO model);
        Task<ResultBase<bool>> DeleteClientAsync(int id);
    }
}

﻿using ApplicationCore.DTO.Products;
using ApplicationCore.Models;
using ApplicationCore.Models.Products;
using System;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IProductsService : IDisposable
    {
        Task<ProductsTableModel> GetProductsAsync(DataTableModel parameters);
        Task<ResultBase<ProductDTO>> GetProductAsync(int id);
        Task<ProductsModel> SearchProductsAsync(string text);
        Task<ResultBase<bool>> DeleteProductAsync(int id);
        Task<ResultBase<bool>> EditProductAsync(ProductDTO model);
    }
}

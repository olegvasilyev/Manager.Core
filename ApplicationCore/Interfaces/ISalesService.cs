﻿using ApplicationCore.DTO.Sales;
using ApplicationCore.Models;
using ApplicationCore.Models.Sales;
using System;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ISalesService : IDisposable
    {
        Task<ResultBase<bool>> EditSaleAsync(SaleDTO sale);
        Task<SalesTableModel> GetSalesAsync(DataTableModel parameters);
        Task<ResultBase<SaleDTO>> GetSaleAsync(int id);
        Task<ResultBase<bool>> DeleteSaleAsync(int id);
    }
}

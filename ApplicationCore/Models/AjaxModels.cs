﻿using Microsoft.AspNetCore.Http;
using System;

namespace ApplicationCore.Models
{
    public static class AjaxParametersFactory
    {
        public static DataTableModel GetDataTableParameters(HttpRequest request)
        {
            var form = request.Form;
            var res = new DataTableModel();
            try
            {
                res.Draw = Convert.ToInt32(form["draw"].ToString());
                res.Skip = Convert.ToInt32(form["start"].ToString());
                res.PageSize = Convert.ToInt32(form["length"].ToString());
                res.ColumnNumber = Convert.ToInt32(form["order[0][column]"].ToString());
                res.ColumnName = form[$"columns[{res.ColumnNumber}][data]"].ToString();
                res.Search = form["search[value]"].ToString();
                res.SortDirection = form["order[0][dir]"].ToString();

                res.ItemID = Convert.ToInt32(form["itemId"].ToString());
                res.ExtraSearch = form["search[extra_search]"].ToString();
            }
            catch (Exception)
            {
            }
            return res;
        }
    }

    public class DataTableModel
    {
        public int Draw { get; set; }
        public int Skip { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public int ColumnNumber { get; set; }
        public string ColumnName { get; set; }
        public string SortDirection { get; set; }
        public int ItemID { get; set; } = 0;
        public string ExtraSearch { get; set; } = "";
    }
}


﻿using ApplicationCore.DTO.Clients;
using System.Collections.Generic;

namespace ApplicationCore.Models.Clients
{
    public class ClientsModel : ResultTableBase<ClientModel> { }

    public class ClientModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string RoleName { get; set; }
        public string Birthday { get; set; }
        public string Passport { get; set; }
        public string ITN { get; set; }
    }

    public class ClientResult : ResultBase<ClientDTO>
    {
        public ClientResult()
        {
            Data = new ClientDTO();
        }
    }
}

﻿namespace ApplicationCore.Models
{
    public static class Constants
    {
        public static string PersonRole = "person";
        public static string EntityRole = "entity";
        public static string AdminRole = "admin";
        public static string UserRole = "user";
        public static string ClientPersonCode = "person";
        public static string ClientEntityCode = "entity";
        public static int SearchCount = 10;
    }
}

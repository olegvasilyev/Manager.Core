﻿using ApplicationCore.DTO.Products;
using System.Collections.Generic;

namespace ApplicationCore.Models.Products
{
    public class ProductsTableModel : ResultTableBase<ProductDTO> { }

    public class ProductsModel : ResultBase<List<ProductDTO>>
    {
        public ProductsModel()
        {
            Data = new List<ProductDTO>();
        }
    }
}

﻿namespace ApplicationCore.Models
{
    public class ResultBase<T>
    {
        public T Data { get; set; }
        public string Message { get; set; } = "";
        public bool IsSuccess { get; set; } = false;
    }
}

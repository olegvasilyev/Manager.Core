﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class ResultTableBase<T>
    {
        public List<T> Data { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public int Draw { get; set; }
        public string Error { get; set; }
    }
}

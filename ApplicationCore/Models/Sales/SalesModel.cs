﻿using ApplicationCore.DTO.Sales;
using System.Collections.Generic;

namespace ApplicationCore.Models.Sales
{
    public class SalesTableModel : ResultTableBase<SaleDTO> { }

    public class SalesModel : ResultBase<List<SaleDTO>>
    {
        public SalesModel()
        {
            Data = new List<SaleDTO>();
        }
    }
}

﻿using ApplicationCore.DTO.Clients;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using ApplicationCore.Models.Clients;
using AutoMapper;
using Infrastructure.Entities;
using InfrastructureL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class ClientsService : IClientsService
    {
        #region System
        private bool _disposed = false;
        private readonly IUnitOfWork _db;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public ClientsService(IUnitOfWork db, IMapper mapper, ILogger<ClientsService> logger)
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        public async Task<ClientsModel> GetClientsAsync(DataTableModel parameters)
        {
            var result = new ClientsModel() { Data = new List<ClientModel>() };

            try
            {
                var items = _db.Clients.GetAll()
                    .Include(x => x.Role)
                    .Include(x => x.LegalEntity)
                    .Include(x => x.Person)
                    .AsQueryable();

                result.RecordsTotal = await items.CountAsync();
                result.Draw = parameters.Draw;
                result.Error = "";

                items = items
                    .Where(x => String.IsNullOrEmpty(parameters.Search) ||
                    x.Role.Name.Contains(parameters.Search) ||
                    x.Country.Contains(parameters.Search) ||
                    x.City.Contains(parameters.Search) ||
                    x.Street.Contains(parameters.Search) ||
                    (x.Person != null && x.Person.FirstName.Contains(parameters.Search)) ||
                    (x.LegalEntity != null && x.LegalEntity.Name.Contains(parameters.Search)));

                switch (parameters.ColumnName)
                {
                    case "name":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Person != null ? x.Person.FullName : x.LegalEntity.Name);
                        else items = items.OrderByDescending(x => x.Person != null ? x.Person.FullName : x.LegalEntity.Name);
                        break;
                    case "role":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Role.Name);
                        else items = items.OrderByDescending(x => x.Role.Name);
                        break;
                    case "address":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Address);
                        else items = items.OrderByDescending(x => x.Address);
                        break;
                    default:
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Id);
                        else items = items.OrderByDescending(x => x.Id);
                        break;
                }

                result.RecordsFiltered = await items.CountAsync();

                result.Data = await items.Skip(parameters.Skip)
                    .Take(parameters.PageSize)
                    .Select(x => new ClientModel
                    {
                        Id = x.Id,
                        Name = x.Role.Code.Equals(Constants.PersonRole) ? x.Person.FullName : x.LegalEntity.Name,
                        Address = x.Address,
                        RoleName = x.Role.Name,
                        Birthday = x.Role.Code.Equals(Constants.PersonRole) ? (x.Person.Birthday.HasValue ? x.Person.Birthday.Value.ToString() : "") : "",
                        Passport = x.Role.Code.Equals(Constants.PersonRole) ? x.Person.Passport ?? "" : "",
                        ITN = x.Role.Code.Equals(Constants.EntityRole) ? x.LegalEntity.ITN.ToString() : ""
                    }).ToListAsync();

            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
                string p = JsonConvert.SerializeObject(parameters);
                _logger.LogCritical(ex, $"parameters={p}");

            }

            return result;
        }

        public async Task<ClientResult> GetClientAsync(int id)
        {
            var result = new ClientResult() { Data = new ClientDTO() };

            try
            {
                var client = await _db.Clients.GetAll()
                      .Include(x => x.Role)
                      .Include(x => x.LegalEntity)
                      .Include(x => x.Person)
                      .FirstOrDefaultAsync(x => x.Id == id);

                if (client != null)
                {
                    result.Data.Id = client.Id;
                    result.Data.Country = client.Country;
                    result.Data.City = client.City;
                    result.Data.Street = client.Street;
                    result.Data.HomeNumber = client.HomeNumber;

                    result.Data.RoleId = client.RoleId;
                    result.Data.RoleCode = client.Role.Code;

                    result.Data.FirstName = client.Person?.FirstName ?? "";
                    result.Data.SurName = client.Person?.SurName ?? "";
                    result.Data.LastName = client.Person?.LastName ?? "";
                    result.Data.Birthday = client.Person?.Birthday ?? null;
                    result.Data.Passport = client.Person?.Passport ?? "";

                    result.Data.Name = client.LegalEntity?.Name ?? "";
                    result.Data.ITN = client.LegalEntity?.ITN ?? "";

                    result.Data.FullName = client.Person?.FullName ?? client.LegalEntity.Name ?? "";

                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }

        public async Task<List<ClientRoleDTO>> GetClientRoles()
        {
            var result = new List<ClientRoleDTO>();

            try
            {
                var items = await _db.ClientRoles.GetAll().ToListAsync();

                if (items != null)
                {
                    result = items.Select(x => new ClientRoleDTO
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "");
            }

            return result;
        }

        public async Task<ResultBase<bool>> DeleteClientAsync(int id)
        {
            var result = new ResultBase<bool>();

            try
            {
                await _db.Clients.DeleteAsync(id);
                await _db.SaveAsync();

                result.Data = true;
                result.Message = "Client was deleted successfully";
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Data = false;
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }

        public async Task<ResultBase<bool>> EditClientAsync(ClientDTO model)
        {
            var result = new ResultBase<bool>();

            try
            {
                var IsModified = false;

                if (model.Id == 0)
                {
                    var client = _mapper.Map<ClientDTO, Client>(model);
                    client.RoleId = model.RoleId;

                    await _db.Clients.CreateAsync(client);

                    var clientRole = _db.ClientRoles.Get(model.RoleId);
                    if (clientRole.Code.Equals(Constants.PersonRole))
                    {
                        var person = _mapper.Map<ClientDTO, Person>(model);
                        person.ClientId = client.Id;

                        await _db.Persons.CreateAsync(person);
                    }
                    else
                    {
                        var entity = _mapper.Map<ClientDTO, LegalEntity>(model);
                        entity.ClientId = client.Id;

                        await _db.LegalEntities.CreateAsync(entity);
                    }

                    IsModified = true;
                }
                else
                {
                    var client = await _db.Clients.GetAll()
                        .Include(x => x.Role)
                        .Include(x => x.Person)
                        .Include(x => x.LegalEntity)
                        .FirstOrDefaultAsync(x => x.Id == model.Id);

                    if (client != null)
                    {
                        if ((client.Country != model.Country) ||
                            (client.City != model.City) ||
                            (client.Street != model.Street) ||
                            client.HomeNumber != model.HomeNumber)
                        {
                            _mapper.Map(model, client, typeof(ClientDTO), typeof(Client));

                            _db.Clients.Update(client);
                        }

                        if (client.Role.Code.Equals(Constants.PersonRole))
                        {
                            var person = client.Person;

                            if ((person.FirstName != model.FirstName) ||
                                (person.SurName != model.SurName) ||
                                (person.LastName != model.LastName) ||
                                (person.Passport != model.Passport) ||
                                person.Birthday != model.Birthday)
                            {
                                _mapper.Map(model, person, typeof(ClientDTO), typeof(Person));

                                _db.Persons.Update(person);
                            }
                        }
                        else
                        {
                            var entity = client.LegalEntity;

                            if (!entity.Name.Equals(model.Name) ||
                                entity.ITN != entity.ITN)
                            {
                                _mapper.Map(model, entity, typeof(ClientDTO), typeof(LegalEntity));

                                _db.LegalEntities.Update(entity);
                            }
                        }

                        IsModified = true;
                    }
                    else
                    {
                        result.Data = false;
                        result.Message = "Client data was not saved";
                    }
                }

                if (IsModified)
                {
                    await _db.SaveAsync();

                    result.Data = true;
                    result.IsSuccess = true;
                    result.Message = "Client data was saved successfully";
                }
            }
            catch (Exception ex)
            {
                result.Data = false;
                result.Message = ex.Message;

                string p = JsonConvert.SerializeObject(model);
                _logger.LogCritical(ex, $"parameters={p}");
            }

            return result;
        }
    }
}

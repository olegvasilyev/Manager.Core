﻿using ApplicationCore.DTO.Products;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using ApplicationCore.Models.Products;
using AutoMapper;
using Infrastructure.Entities;
using InfrastructureL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class ProductsService : IProductsService
    {
        #region System
        private bool _disposed = false;
        private readonly IUnitOfWork _db;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public ProductsService(IUnitOfWork db, IMapper mapper, ILogger<ProductsService> logger)
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public async Task<ProductsTableModel> GetProductsAsync(DataTableModel parameters)
        {
            var result = new ProductsTableModel();

            try
            {
                var items = _db.Products.GetAll();

                result.RecordsTotal = await items.CountAsync();
                result.Draw = parameters.Draw;
                result.Error = "";

                items = items.Where(x => String.IsNullOrEmpty(parameters.Search) || x.Name.Contains(parameters.Search));

                switch (parameters.ColumnName)
                {
                    case "name":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Name);
                        else items = items.OrderByDescending(x => x.Name);
                        break;
                    case "price":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Price);
                        else items = items.OrderByDescending(x => x.Price);
                        break;
                    case "count":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Count);
                        else items = items.OrderByDescending(x => x.Count);
                        break;
                    default:
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Id);
                        else items = items.OrderByDescending(x => x.Id);
                        break;
                }

                result.RecordsFiltered = await items.CountAsync();

                result.Data = await items.Skip(parameters.Skip)
                    .Take(parameters.PageSize)
                    .Select(x => new ProductDTO
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Price = x.Price,
                        Count = x.Count
                    }).ToListAsync();
            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
                string p = JsonConvert.SerializeObject(parameters);
                _logger.LogCritical(ex, $"parameters={p}");
            }

            return result;
        }

        public async Task<ResultBase<ProductDTO>> GetProductAsync(int id)
        {
            var result = new ResultBase<ProductDTO>();

            try
            {
                var product = await _db.Products.GetAsync(id);

                if (product != null)
                {
                    result.Data = _mapper.Map<ProductDTO>(product);

                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }

        public async Task<ProductsModel> SearchProductsAsync(string text)
        {
            var result = new ProductsModel();

            try
            {
                var items = await _db.Products.GetAll()
                    .Where(x => x.Name.Contains(text) && x.Count > 0)
                    .OrderBy(x => x.Name)
                    .Take(Constants.SearchCount)
                    .ToListAsync();

                result.Data = items.Select(x => new ProductDTO
                {
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    Count = x.Count
                }).ToList();

                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"text={text}");
            }

            return result;
        }

        public async Task<ResultBase<bool>> DeleteProductAsync(int id)
        {
            var result = new ResultBase<bool>();

            try
            {
                await _db.Products.DeleteAsync(id);
                await _db.SaveAsync();

                result.Data = true;
                result.Message = "Product was deleted successfully";
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }

        public async Task<ResultBase<bool>> EditProductAsync(ProductDTO model)
        {
            var result = new ResultBase<bool>();

            try
            {
                var isModified = false;

                if (model.Id == 0)
                {
                    var product = _mapper.Map<ProductDTO, Product>(model);

                    await _db.Products.CreateAsync(product);

                    isModified = true;
                }
                else
                {
                    var product = _db.Products.Get(model.Id);

                    if (product != null)
                    {
                        if ((product.Name != model.Name) ||
                            product.Price != model.Price ||
                            product.Count != model.Count)
                        {
                            _mapper.Map(model, product, typeof(ProductDTO), typeof(Product));

                            _db.Products.Update(product);

                            isModified = true;
                        }
                    }
                    else
                    {
                        result.Data = false;
                        result.Message = "Product data was not saved";
                    }
                }

                if (isModified)
                {
                    await _db.SaveAsync();

                    result.Data = true;
                    result.IsSuccess = true;
                    result.Message = "Product data was saved successfully";
                }
            }
            catch (Exception ex)
            {
                result.Data = false;
                result.Message = ex.Message;

                string p = JsonConvert.SerializeObject(model);
                _logger.LogCritical(ex, $"parameters={p}");
            }

            return result;
        }
    }
}

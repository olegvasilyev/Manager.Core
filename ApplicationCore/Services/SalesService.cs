﻿using ApplicationCore.DTO.Sales;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using ApplicationCore.Models.Sales;
using AutoMapper;
using Infrastructure.Entities;
using InfrastructureL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class SalesService : ISalesService
    {
        #region System
        private bool _disposed = false;
        private IUnitOfWork _db;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public SalesService(IUnitOfWork db, IMapper mapper, ILogger<SalesService> logger)
        {
            _db = db;
            _logger = logger;
            _mapper = mapper;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public async Task<SalesTableModel> GetSalesAsync(DataTableModel parameters)
        {
            var result = new SalesTableModel();

            try
            {
                var items = _db.Sales.GetAll();

                result.RecordsTotal = await items.CountAsync();
                result.Draw = parameters.Draw;
                result.Error = "";

                items = items.Where(x =>
                    (String.IsNullOrEmpty(parameters.Search) || x.Product.Name.Contains(parameters.Search))
                    && (parameters.ItemID == 0 || x.ClientId == parameters.ItemID));

                switch (parameters.ColumnName)
                {
                    case "saleDateStr":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.SaleDate);
                        else items = items.OrderByDescending(x => x.SaleDate);
                        break;
                    case "productName":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.ProductName);
                        else items = items.OrderByDescending(x => x.ProductName);
                        break;
                    case "count":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Count);
                        else items = items.OrderByDescending(x => x.Count);
                        break;
                    case "totalCost":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.TotalCost);
                        else items = items.OrderByDescending(x => x.TotalCost);
                        break;
                    default:
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Id);
                        else items = items.OrderByDescending(x => x.Id);
                        break;
                }

                result.RecordsFiltered = await items.CountAsync();

                result.Data = await items.Skip(parameters.Skip)
                    .Take(parameters.PageSize)
                    .Select(x => new SaleDTO
                    {
                        Id = x.Id,
                        SaleDate = x.SaleDate,
                        Count = x.Count,
                        TotalCost = x.TotalCost,
                        ClientName = x.ClientName,
                        ProductName = x.ProductName,
                        ClientId = x.ClientId,
                        ProductId = x.ProductId
                    }).ToListAsync();

            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
                string p = JsonConvert.SerializeObject(parameters);
                _logger.LogCritical(ex, $"parameters={p}");
            }

            return result;
        }

        public async Task<ResultBase<bool>> EditSaleAsync(SaleDTO sale)
        {
            var result = new ResultBase<bool> { Data = false };

            try
            {
                var isModified = false;
                var message = "";

                var client = await _db.Clients.GetAll()
                    .Include(x => x.Person)
                    .Include(x => x.LegalEntity)
                    .FirstOrDefaultAsync(x => x.Id == sale.ClientId);

                var product = await _db.Products.GetAsync(sale.ProductId);

                if (client != null && product != null)
                {
                    if (sale.Id == 0)
                    {
                        if (product.Count >= sale.Count)
                        {
                            product.Count -= sale.Count;
                            _db.Products.Update(product);

                            var newSale = new Sale
                            {
                                Id = 0,
                                SaleDate = DateTime.Now,
                                Count = sale.Count,
                                TotalCost = product.Price * sale.Count,
                                ClientName = client.Person?.FullName ?? client.LegalEntity?.Name ?? "",
                                ProductName = product.Name,
                                ClientId = client.Id,
                                ProductId = product.Id
                            };

                            _db.Sales.Create(newSale);

                            isModified = true;
                        }
                        else
                        {
                            message = "Available products count less than requested";
                        }
                    }
                    else
                    {
                        var processSale = await _db.Sales.GetAsync(sale.Id);

                        if (processSale != null && processSale.Count != sale.Count)
                        {
                            var difference = sale.Count - processSale.Count;
                            var checkCount = product.Count - difference;

                            if (checkCount >= 0)
                            {
                                product.Count -= difference;
                                _db.Products.Update(product);

                                processSale.Count = sale.Count;
                                processSale.TotalCost = product.Price * sale.Count;

                                _db.Sales.Update(processSale);
                                isModified = true;
                            }
                            else
                            {
                                message = "Available products count less than requested";
                            }
                        }
                        else
                        {
                            message = "There are no changes to save or request has wrong data";
                        }
                    }
                }

                if (isModified)
                {
                    await _db.SaveAsync();

                    result.Data = true;
                    result.Message = "Sale operation was saved successfully";
                    result.IsSuccess = true;
                }
                else
                {
                    result.Message = String.IsNullOrEmpty(message) ? "Operation was aborted!" : message;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                string p = JsonConvert.SerializeObject(sale);
                _logger.LogCritical(ex, $"parameters={p}");
            }

            return result;
        }

        public async Task<ResultBase<SaleDTO>> GetSaleAsync(int id)
        {
            var result = new ResultBase<SaleDTO>();

            try
            {
                var sale = await _db.Sales.GetAll()
                    .Include(x => x.Product)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (sale != null)
                {
                    result.Data = _mapper.Map<SaleDTO>(sale);

                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }

        public async Task<ResultBase<bool>> DeleteSaleAsync(int id)
        {
            var result = new ResultBase<bool>();

            try
            {
                var sale = _db.Sales.Get(id);
                if (sale != null)
                {
                    var product = _db.Products.Get(sale.ProductId);
                    product.Count += sale.Count;
                    _db.Products.Update(product);
                }

                await _db.Sales.DeleteAsync(id);
                await _db.SaveAsync();

                result.Data = true;
                result.Message = "Sale was deleted successfully";
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                _logger.LogCritical(ex, $"id={id}");
            }

            return result;
        }
    }
}

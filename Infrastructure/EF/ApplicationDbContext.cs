﻿using Infrastructure.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EF
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        #region Context properties
        public virtual DbSet<ClientRole> ClientRoles { get; set; }

        public virtual DbSet<Client> Clients { get; set; }

        public virtual DbSet<LegalEntity> LegalEntities { get; set; }

        public virtual DbSet<Person> Persons { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Sale> Sales { get; set; } 
        #endregion

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer(@"Data Source=MEDIASTATION;Initial Catalog=ManagerDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        //    }
        //}

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Entity<ClientRole>(entity =>
        //    {
        //        entity.Property(e => e.Name)
        //            .IsRequired()
        //            .HasMaxLength(32);

        //        entity.Property(e => e.Code)
        //            .IsRequired()
        //            .HasMaxLength(32);
        //    });

        //    modelBuilder.Entity<Client>(entity =>
        //    {
        //        entity.HasIndex(e => e.RoleId)
        //            .HasName("NonClusteredIndex-ClientRoleFK");

        //        entity.Property(e => e.City)
        //            .IsRequired()
        //            .HasMaxLength(64);

        //        entity.Property(e => e.Country)
        //            .IsRequired()
        //            .HasMaxLength(64);

        //        entity.Property(e => e.HomeNumber)
        //            .IsRequired()
        //            .HasMaxLength(8);

        //        entity.Property(e => e.RoleId).HasColumnName("RoleID");

        //        entity.Property(e => e.Street)
        //            .IsRequired()
        //            .HasMaxLength(64);

        //        entity.HasOne(d => d.Role)
        //            .WithMany(p => p.Clients)
        //            .HasForeignKey(d => d.RoleId)
        //            .HasConstraintName("FK_Clients_ClientRoles");
        //    });

        //    modelBuilder.Entity<LegalEntity>(entity =>
        //    {
        //        entity.HasIndex(e => e.ClientId)
        //            .HasName("NonClusteredIndex-ClientFK");

        //        entity.Property(e => e.ClientId).HasColumnName("ClientID");

        //        entity.Property(e => e.Itn).HasColumnName("ITN");

        //        entity.Property(e => e.Name)
        //            .IsRequired()
        //            .HasMaxLength(128);

        //        entity.HasOne(d => d.Client)
        //            .WithMany(p => p.LegalEntities)
        //            .HasForeignKey(d => d.ClientId)
        //            .HasConstraintName("FK_LegalEntities_Clients");
        //    });

        //    modelBuilder.Entity<Person>(entity =>
        //    {
        //        entity.HasIndex(e => e.ClientId)
        //            .HasName("NonClusteredIndex-ClientFK");

        //        entity.Property(e => e.Birthday).HasColumnType("date");

        //        entity.Property(e => e.ClientId).HasColumnName("ClientID");

        //        entity.Property(e => e.FirstName)
        //            .IsRequired()
        //            .HasMaxLength(16);

        //        entity.Property(e => e.LastName)
        //            .IsRequired()
        //            .HasMaxLength(16);

        //        entity.Property(e => e.Passport).HasMaxLength(16);

        //        entity.Property(e => e.SurName)
        //            .IsRequired()
        //            .HasMaxLength(16);

        //        entity.HasOne(d => d.Client)
        //            .WithMany(p => p.Persons)
        //            .HasForeignKey(d => d.ClientId)
        //            .HasConstraintName("FK_Persons_Clients");
        //    });

        //    modelBuilder.Entity<Product>(entity =>
        //    {
        //        entity.Property(e => e.Name)
        //            .IsRequired()
        //            .HasMaxLength(256);
        //    });

        //    modelBuilder.Entity<Sale>(entity =>
        //    {
        //        entity.HasIndex(e => e.ClientId)
        //            .HasName("NonClusteredIndex-ClientFK");

        //        entity.HasIndex(e => e.ProductId)
        //            .HasName("NonClusteredIndex-ProductFK");

        //        entity.Property(e => e.ClientId).HasColumnName("ClientID");

        //        entity.Property(e => e.ClientName)
        //            .IsRequired()
        //            .HasMaxLength(128);

        //        entity.Property(e => e.ProductId).HasColumnName("ProductID");

        //        entity.Property(e => e.ProductName)
        //            .IsRequired()
        //            .HasMaxLength(256);

        //        entity.Property(e => e.SaleDate).HasColumnType("datetime");

        //        entity.HasOne(d => d.Client)
        //            .WithMany(p => p.Sales)
        //            .HasForeignKey(d => d.ClientId)
        //            .OnDelete(DeleteBehavior.SetNull)
        //            .HasConstraintName("FK_Sales_Clients");

        //        entity.HasOne(d => d.Product)
        //            .WithMany(p => p.Sales)
        //            .HasForeignKey(d => d.ProductId)
        //            .OnDelete(DeleteBehavior.SetNull)
        //            .HasConstraintName("FK_Sales_Products");
        //    });
        //}
    }
}

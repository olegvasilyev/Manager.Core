﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure.Entities
{
    public partial class Client
    {
        public Client()
        {
            Sales = new HashSet<Sale>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Country { get; set; }

        [Required]
        [MaxLength(32)]
        public string City { get; set; }

        [Required]
        [MaxLength(32)]
        public string Street { get; set; }

        [MaxLength(16)]
        public string HomeNumber { get; set; }

        [NotMapped]
        public string Address
        {
            get
            {
                return $"{Street} {HomeNumber ?? ""} {City}, {Country}";
            }

        }

        public int RoleId { get; set; }

        public ClientRole Role { get; set; }

        public LegalEntity LegalEntity { get; set; }

        public Person Person { get; set; }

        public ICollection<Sale> Sales { get; set; }
    }
}

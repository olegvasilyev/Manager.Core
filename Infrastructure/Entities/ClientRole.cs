﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Entities
{
    public partial class ClientRole
    {
        public ClientRole()
        {
            Clients = new HashSet<Client>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required]
        [MaxLength(32)]
        public string Code { get; set; }

        public ICollection<Client> Clients { get; set; }
    }
}

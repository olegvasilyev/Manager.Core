﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Entities
{
    public partial class LegalEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(16)]
        public string ITN { get; set; }

        public int ClientId { get; set; }

        public Client Client { get; set; }
    }
}

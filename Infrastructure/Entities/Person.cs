﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infrastructure.Entities
{
    public partial class Person
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(16)]
        public string FirstName { get; set; }

        [MaxLength(16)]
        public string SurName { get; set; }

        [MaxLength(16)]
        public string LastName { get; set; }

        public int ClientId { get; set; }

        [Column(TypeName = "date ")]
        public DateTime? Birthday { get; set; }

        [MaxLength(16)]
        public string Passport { get; set; }

        public Client Client { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                var fullName = new StringBuilder("");

                fullName.Append(String.IsNullOrEmpty(SurName) ? "" : $"{SurName} ");
                fullName.Append(String.IsNullOrEmpty(FirstName) ? "" : $"{FirstName} ");
                fullName.Append(String.IsNullOrEmpty(LastName) ? "" : $"{LastName}");

                return fullName.ToString().Trim();
            }
        }

    }
}

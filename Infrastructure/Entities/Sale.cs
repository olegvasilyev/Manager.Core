﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure.Entities
{
    public partial class Sale
    {
        [Key]
        public int Id { get; set; }

        public DateTime SaleDate { get; set; }

        public int Count { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalCost { get; set; }

        public string ClientName { get; set; }

        public string ProductName { get; set; }

        public int ClientId { get; set; }

        public int ProductId { get; set; }

        public Client Client { get; set; }

        public Product Product { get; set; }
    }
}

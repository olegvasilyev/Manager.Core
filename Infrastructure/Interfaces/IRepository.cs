﻿using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);

        Task<T> GetAsync(int id);
        Task CreateAsync(T item);
        Task DeleteAsync(int id);
    }
}

﻿using Infrastructure.Entities;
using Infrastructure.Interfaces;
using System;
using System.Threading.Tasks;

namespace InfrastructureL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Client> Clients { get; }
        IRepository<ClientRole> ClientRoles { get; }
        IRepository<LegalEntity> LegalEntities { get; }
        IRepository<Person> Persons { get; }
        IRepository<Product> Products { get; }
        IRepository<Sale> Sales { get; }
        void Save();
        Task SaveAsync();
    }
}

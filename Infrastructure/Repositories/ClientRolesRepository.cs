﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ClientRolesRepository : IRepository<ClientRole>
    {
        private ApplicationDbContext _db;

        public ClientRolesRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(ClientRole item)
        {
            _db.ClientRoles.Add(item);
        }

        public void Delete(int id)
        {
            ClientRole item = Get(id);
            if (item != null)
                _db.ClientRoles.Remove(item);
        }

        public ClientRole Get(int id)
        {
            return _db.ClientRoles.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<ClientRole> GetAll()
        {
            return _db.ClientRoles;
        }

        public void Update(ClientRole item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(ClientRole item)
        {
            await _db.ClientRoles.AddAsync(item);
        }

        public async Task<ClientRole> GetAsync(int id)
        {
            return await _db.ClientRoles.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            ClientRole item = await GetAsync(id);
            if (item != null)
                _db.ClientRoles.Remove(item);
        }
    }
}

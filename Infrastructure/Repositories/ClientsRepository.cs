﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ClientsRepository : IRepository<Client>
    {
        private ApplicationDbContext _db;

        public ClientsRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(Client item)
        {
            _db.Clients.Add(item);
        }

        public void Delete(int id)
        {
            Client item = Get(id);
            if (item != null)
                _db.Clients.Remove(item);
        }

        public Client Get(int id)
        {
            return _db.Clients.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Client> GetAll()
        {
            return _db.Clients;
        }

        public void Update(Client item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(Client item)
        {
            await _db.Clients.AddAsync(item);
        }

        public async Task<Client> GetAsync(int id)
        {
            return await _db.Clients.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            Client item = await GetAsync(id);
            if (item != null)
                _db.Clients.Remove(item);
        }
    }
}

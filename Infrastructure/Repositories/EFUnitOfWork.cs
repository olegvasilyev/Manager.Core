﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using InfrastructureL.Interfaces;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;
        private ApplicationDbContext _db;
        private ClientsRepository _clientsRepository;
        private ClientRolesRepository _clientRolesRepository;
        private LegalEntitiesRepository _legalEntitiesRepository;
        private PersonsRepository _personsRepository;
        private ProductsRepository _productsRepository;
        private SalesRepository _salesRepository;

        public EFUnitOfWork(ApplicationDbContext context)
        {
            _db = context;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
        
        public IRepository<Client> Clients
        {
            get
            {
                if (_clientsRepository == null)
                    _clientsRepository = new ClientsRepository(_db);
                return _clientsRepository;
            }
        }

        public IRepository<ClientRole> ClientRoles
        {
            get
            {
                if (_clientRolesRepository == null)
                    _clientRolesRepository = new ClientRolesRepository(_db);
                return _clientRolesRepository;
            }
        }

        public IRepository<LegalEntity> LegalEntities
        {
            get
            {
                if (_legalEntitiesRepository == null)
                    _legalEntitiesRepository = new LegalEntitiesRepository(_db);
                return _legalEntitiesRepository;
            }
        }

        public IRepository<Person> Persons
        {
            get
            {
                if (_personsRepository == null)
                    _personsRepository = new PersonsRepository(_db);
                return _personsRepository;
            }
        }

        public IRepository<Sale> Sales
        {
            get
            {
                if (_salesRepository == null)
                    _salesRepository = new SalesRepository(_db);
                return _salesRepository;
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                if (_productsRepository == null)
                    _productsRepository = new ProductsRepository(_db);
                return _productsRepository;
            }
        }
    }
}

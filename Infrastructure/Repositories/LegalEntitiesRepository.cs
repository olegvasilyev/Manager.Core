﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class LegalEntitiesRepository : IRepository<LegalEntity>
    {
        private ApplicationDbContext _db;

        public LegalEntitiesRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(LegalEntity item)
        {
            _db.LegalEntities.Add(item);
        }

        public void Delete(int id)
        {
            LegalEntity item = Get(id);
            if (item != null)
                _db.LegalEntities.Remove(item);
        }

        public LegalEntity Get(int id)
        {
            return _db.LegalEntities.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<LegalEntity> GetAll()
        {
            return _db.LegalEntities;
        }

        public void Update(LegalEntity item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(LegalEntity item)
        {
            await _db.LegalEntities.AddAsync(item);
        }

        public async Task<LegalEntity> GetAsync(int id)
        {
            return await _db.LegalEntities.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            LegalEntity item = await GetAsync(id);
            if (item != null)
                _db.LegalEntities.Remove(item);
        }
    }
}

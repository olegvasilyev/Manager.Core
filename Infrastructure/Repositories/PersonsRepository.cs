﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class PersonsRepository : IRepository<Person>
    {
        private ApplicationDbContext _db;

        public PersonsRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(Person item)
        {
            _db.Persons.Add(item);
        }

        public void Delete(int id)
        {
            Person item = Get(id);
            if (item != null)
                _db.Persons.Remove(item);
        }

        public Person Get(int id)
        {
            return _db.Persons.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Person> GetAll()
        {
            return _db.Persons;
        }

        public void Update(Person item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(Person item)
        {
            await _db.Persons.AddAsync(item);
        }

        public async Task<Person> GetAsync(int id)
        {
            return await _db.Persons.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            Person item = await GetAsync(id);
            if (item != null)
                _db.Persons.Remove(item);
        }
    }
}

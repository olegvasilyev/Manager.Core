﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ProductsRepository : IRepository<Product>
    {
        private ApplicationDbContext _db;

        public ProductsRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(Product item)
        {
            _db.Products.Add(item);
        }

        public void Delete(int id)
        {
            Product item = Get(id);
            if (item != null)
                _db.Products.Remove(item);
        }

        public Product Get(int id)
        {
            return _db.Products.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Product> GetAll()
        {
            return _db.Products;
        }

        public void Update(Product item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(Product item)
        {
            await _db.Products.AddAsync(item);
        }

        public async Task<Product> GetAsync(int id)
        {
            return await _db.Products.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            Product item = await GetAsync(id);
            if (item != null)
                _db.Products.Remove(item);
        }
    }
}

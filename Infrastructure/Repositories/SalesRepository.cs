﻿using Infrastructure.EF;
using Infrastructure.Entities;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class SalesRepository : IRepository<Sale>
    {
        private ApplicationDbContext _db;

        public SalesRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public void Create(Sale item)
        {
            _db.Sales.Add(item);
        }

        public void Delete(int id)
        {
            Sale item = Get(id);
            if (item != null)
                _db.Sales.Remove(item);
        }

        public Sale Get(int id)
        {
            return _db.Sales.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Sale> GetAll()
        {
            return _db.Sales;
        }

        public void Update(Sale item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public async Task CreateAsync(Sale item)
        {
            await _db.Sales.AddAsync(item);
        }

        public async Task<Sale> GetAsync(int id)
        {
            return await _db.Sales.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            Sale item = await GetAsync(id);
            if (item != null)
                _db.Sales.Remove(item);
        }
    }
}

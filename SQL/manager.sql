/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 12.03.2018 1:34:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[UserName] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientRoles]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](32) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_ClientRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[City] [nvarchar](32) NOT NULL,
	[Country] [nvarchar](32) NOT NULL,
	[HomeNumber] [nvarchar](16) NULL,
	[RoleId] [int] NOT NULL,
	[Street] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LegalEntities]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LegalEntities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ITN] [nvarchar](16) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_LegalEntities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Persons]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Birthday] [date] NULL,
	[ClientId] [int] NOT NULL,
	[FirstName] [nvarchar](16) NOT NULL,
	[LastName] [nvarchar](16) NULL,
	[Passport] [nvarchar](16) NULL,
	[SurName] [nvarchar](16) NULL,
 CONSTRAINT [PK_Persons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Count] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sales]    Script Date: 12.03.2018 1:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](max) NULL,
	[Count] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ProductName] [nvarchar](max) NULL,
	[SaleDate] [datetime2](7) NOT NULL,
	[TotalCost] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[AspNetRoles] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (N'749198b1-a12a-42eb-9cba-5d80f446a902', N'0b309b41-635b-4717-96ad-a8c36f82e12e', N'user', N'USER')
INSERT [dbo].[AspNetRoles] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (N'aee170ff-b17f-4856-a821-302300c19001', N'7da9dbbe-e5b1-4eb1-a48e-31102b753e39', N'admin', N'ADMIN')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd06b87bc-5993-4600-9098-32d04716fcfa', N'749198b1-a12a-42eb-9cba-5d80f446a902')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'30c3ab84-877d-4bb3-98a2-4b2e5e192426', N'aee170ff-b17f-4856-a821-302300c19001')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'abf9aa9f-7d27-4408-83bf-c6a4af2bf9d8', N'aee170ff-b17f-4856-a821-302300c19001')
INSERT [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (N'30c3ab84-877d-4bb3-98a2-4b2e5e192426', 0, N'ee5e4463-8832-4767-a83d-4fc3cb1e81eb', N'testadmin@gmail.com', 0, 1, NULL, N'TESTADMIN@GMAIL.COM', N'TESTADMIN@GMAIL.COM', N'AQAAAAEAACcQAAAAECHDM9BNOx/fiUKUoGSzm3m8i2jKJ+Z5OBrmZ4Kxdy2Gzb4HO3MpzSsTXbv8X/A1RA==', NULL, 0, N'3cc27e02-245a-4db3-aa35-2a61ecfbbaa6', 0, N'testadmin@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (N'abf9aa9f-7d27-4408-83bf-c6a4af2bf9d8', 0, N'f7d18e1d-6c48-4525-9941-9927f7b85f4c', N'admin@admin.com', 0, 1, NULL, N'ADMIN@ADMIN.COM', N'ADMIN@ADMIN.COM', N'AQAAAAEAACcQAAAAEHlzPdSYWe6tiGDMhteu2M46GL792z256eItLH7sxfMigvXQR4uB/A6tTNIERYyaXA==', NULL, 0, N'9933f39f-4a6e-4fe7-b2e9-ae2b02bc0efd', 0, N'admin@admin.com')
INSERT [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName]) VALUES (N'd06b87bc-5993-4600-9098-32d04716fcfa', 0, N'5383dcfc-b3b5-4838-8c71-8644014ecb74', N'testuser@gmail.com', 0, 1, NULL, N'TESTUSER@GMAIL.COM', N'TESTUSER@GMAIL.COM', N'AQAAAAEAACcQAAAAEAOMfleIROr2eHnzkJ5k+Syx5O90YJqBaRjwg65VTFEtzOalXAxetREeSue2+jyUsQ==', NULL, 0, N'7b9b0bbb-4c44-4a1d-b574-b2a4e7a10980', 0, N'testuser@gmail.com')
SET IDENTITY_INSERT [dbo].[ClientRoles] ON 

INSERT [dbo].[ClientRoles] ([Id], [Code], [Name]) VALUES (1, N'person', N'Person')
INSERT [dbo].[ClientRoles] ([Id], [Code], [Name]) VALUES (2, N'entity', N'Legal entity')
SET IDENTITY_INSERT [dbo].[ClientRoles] OFF
SET IDENTITY_INSERT [dbo].[Clients] ON 

INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (1, N'London', N'United Kingdom', N'221b', 1, N'Baker')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (2, N'London', N'United Kingdom', N'221b', 1, N'Baker')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (3, N'Redmond', N'USA', N'123', 2, N'Soft')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (4, N'Mountain View', N'USA', N'1600', 2, N'Amphitheatre Parkway ')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (43, N'Weston-super-Mare', N'England', NULL, 1, N'Somerset')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (49, N'Stockholm', N'Sweden', NULL, 1, N'unknown')
INSERT [dbo].[Clients] ([Id], [City], [Country], [HomeNumber], [RoleId], [Street]) VALUES (50, N'New York', N'USA', NULL, 1, N'Westbury')
SET IDENTITY_INSERT [dbo].[Clients] OFF
SET IDENTITY_INSERT [dbo].[LegalEntities] ON 

INSERT [dbo].[LegalEntities] ([Id], [ClientId], [ITN], [Name]) VALUES (1, 3, N'123664123', N'Microsoft')
INSERT [dbo].[LegalEntities] ([Id], [ClientId], [ITN], [Name]) VALUES (2, 4, N'546698775', N'Google')
SET IDENTITY_INSERT [dbo].[LegalEntities] OFF
SET IDENTITY_INSERT [dbo].[Persons] ON 

INSERT [dbo].[Persons] ([Id], [Birthday], [ClientId], [FirstName], [LastName], [Passport], [SurName]) VALUES (4, CAST(N'1854-12-01' AS Date), 1, N'Sherlock', NULL, N'123456789', N'Holmes')
INSERT [dbo].[Persons] ([Id], [Birthday], [ClientId], [FirstName], [LastName], [Passport], [SurName]) VALUES (5, CAST(N'1851-10-01' AS Date), 2, N'Watson', NULL, N'456997455', N'dr.')
INSERT [dbo].[Persons] ([Id], [Birthday], [ClientId], [FirstName], [LastName], [Passport], [SurName]) VALUES (22, CAST(N'1945-04-14' AS Date), 43, N'Richard', N'Hugh', NULL, N'Blackmore')
INSERT [dbo].[Persons] ([Id], [Birthday], [ClientId], [FirstName], [LastName], [Passport], [SurName]) VALUES (24, NULL, 49, N'Yngwie', N'Johan', NULL, N'Malmsteen')
INSERT [dbo].[Persons] ([Id], [Birthday], [ClientId], [FirstName], [LastName], [Passport], [SurName]) VALUES (25, CAST(N'1956-07-15' AS Date), 50, N'Joe', N'Satch', NULL, N'Satriani')
SET IDENTITY_INSERT [dbo].[Persons] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (1, 3, N'Guitar Donner DAG-1C Beginner Acoustic Guitar Full Size, 41" Cutaway Guitar Bundle with Gig Bag Tuner Capo Picks Strap String', CAST(129.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (2, 2, N'Ibanez S670QM S Series Electric Guitar Sapphire Blue', CAST(599.99 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (3, 4, N'Ibanez RG Premium RG1070PBZ Electric Guitar Cerulean Blue Burst', CAST(1299.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (4, 3, N'Ibanez JEM7V Steve Vai Signature Electric Guitar White', CAST(2999.99 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (5, 2, N'Ibanez JEM77 Steve Vai Signature - Blue Floral Pattern', CAST(2599.99 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (6, 4, N'Ibanez Prestige RG655M 6-String Electric Guitar with Case, Basswood Body, 24 Frets, Maple/Walnut Neck, Maple Fretboard, Passive Pickup, White', CAST(1199.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (7, 1, N'Ibanez JS24P Joe Satriani Signature - Candy Apple Red', CAST(1499.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (8, 2, N'Fender American Vintage ''52 Telecaster, Maple Fingerboard - Butterscotch Blonde', CAST(1749.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (9, 2, N'Fender Standard Stratocaster HSH PF Black Bundle W/ Fender Gig Bag, Stand, Tuner and Instrument Cable', CAST(687.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (10, 10, N'Squier by Fender Short Scale Stratocaster Pack with Frontman 10G Amp, Cable, Strap, Picks, and Online Lessons - Brown Sunburst Bundle with Austin Bazaar Instructional DVD', CAST(219.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (11, 50, N'Fender 351 Shape Classic Picks (12 Pack) for electric guitar, acoustic guitar, mandolin, and bass', CAST(4.99 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (12, 3, N'Fender 145102550 Standard Telecaster Electric Guitar - Maple Fingerboard - Butterscotch Blonde', CAST(599.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (13, 3, N'Gibson Les Paul Traditional 2016 T Electric Guitar Premium Finish, Light Burst', CAST(1573.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (14, 5, N'Gibson USA HLPS17B8CH1 Les Paul Standard HP 2017 Electric Guitar, Bourbon Burst', CAST(2540.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (15, 1, N'Gibson J-15 Acoustic-Electric Guitar', CAST(1174.91 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (16, 2, N'Gibson Les Paul Faded T 2017, Worn Brown', CAST(1189.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (17, 2, N'Gibson Les Paul Studio 2016 T Electric Guitar, Alpine White', CAST(1544.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (18, 3, N'Gibson Les Paul Studio 2016, High Performance - Ebony', CAST(1789.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (19, 10, N'
Yamaha C40II BL Classical Guitar Limited Edition Black', CAST(139.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (20, 1, N'Yamaha C40 Full Size Nylon-String Classical Guitar', CAST(139.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (21, 1, N'Yamaha L-Series Transacoustic Guitar - Concert Size, Vintage Natural', CAST(999.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (22, 3, N'Yamaha FS-TA Concert Size Transacoustic Guitar w/ Chorus and Reverb, Ruby Red', CAST(599.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (23, 2, N'Yamaha Pacifica PAC611HFM LAB Solid-Body Electric Guitar, Light Amber Burst', CAST(610.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (24, 1, N'Apple MacBook Pro MGXA2LL/A 15-Inch Laptop with Retina Display (2.2 GHz Intel Core i7 Processor, 16 GB RAM, 256 GB HDD)', CAST(1999.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (25, 4, N'2017 HP Notebook 15.6 Inch Premium Flagship High Performance Laptop Computer (Intel Core i7-7500U 2.7GHz up to 3.5GHz, 16GB RAM, 128GB SSD, DVD, WiFi, HD Webcam, Windows 10 Home) Silver', CAST(699.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (27, 3, N'2018 Dell Business Flagship Laptop Notebook 15.6" HD+ LED-Backlit Display Intel i5-7200U Processor 8GB DDR4 RAM 256GB HDD DVD-RW HDMI Webcam Bluetooth Windows 10 Pro-Black', CAST(649.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (28, 2, N'HP 17.3" HD+ Notebook (2018 New), Intel Core i3-7100U Processor 2.4 GHz, 8GB Memory, 2TB Hard Drive, Optical Drive, HD Webcam, Backlit Keyboard, Windows 10 Home, Marine Blue', CAST(589.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (29, 1, N'HP 17.3" HD+ Notebook (2018 New), Intel Core i3-7100U Processor 2.4 GHz, 8GB Memory, 2TB Hard Drive, Optical Drive, HD Webcam, Backlit Keyboard, Windows 10 Home, Pale Mint', CAST(627.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (30, 1, N'Envy x360 2018 HP Ryzen Micro-edge 2-in-1 Flagship Notebook | 15.6" FHD MultiTouch Display | 4-Core AMD Ryzen 5 2500U Up to 3.6Ghz | 8GB DDR4 | 1TB HDD | Webcam | Backlit Keyboard | Radeon Vega', CAST(769.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (31, 2, N'Apple MacBook Pro 13 (Mid 2012) - Core i5 2.5GHz, 4GB RAM, 500GB HDD', CAST(1191.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (32, 2, N'Apple iBook Laptop 14.1" M9388LL/A (933-MHz PowerPC G4, 256 MB RAM, 40 GB Hard Drive, DVD/CD-RW Drive)', CAST(970.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (33, 1, N'ibook G4 12" 1.2 GHZ, 512MB RAM, 30 GB HD, COMBO DRIVE, AIRPORT EXTREME', CAST(1999.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (34, 2, N'Spector Basses CODA4PROBC Coda 4 Pro Bass Guitar, Black Cherry', CAST(899.00 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (35, 1, N'Fender Standard Jazz Electric Bass Guitar - Pau Ferro Fingerboard, Brown Sunburst', CAST(624.99 AS Decimal(18, 2)))
INSERT [dbo].[Products] ([Id], [Count], [Name], [Price]) VALUES (36, 4, N'Schecter Stiletto Extreme-5 Bass Guitar, Black Cherry', CAST(524.99 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[Sales] ON 

INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (31, 4, N'Google', 2, 27, N'2018 Dell Business Flagship Laptop Notebook 15.6" HD+ LED-Backlit Display Intel i5-7200U Processor 8GB DDR4 RAM 256GB HDD DVD-RW HDMI Webcam Bluetooth Windows 10 Pro-Black', CAST(N'2018-03-11T18:57:30.3992498' AS DateTime2), CAST(1298.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (34, 50, N'Satriani Joe Satch', 1, 7, N'Ibanez JS24P Joe Satriani Signature - Candy Apple Red', CAST(N'2018-03-12T00:26:17.5370206' AS DateTime2), CAST(1499.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (35, 50, N'Satriani Joe Satch', 1, 32, N'Apple iBook Laptop 14.1" M9388LL/A (933-MHz PowerPC G4, 256 MB RAM, 40 GB Hard Drive, DVD/CD-RW Drive)', CAST(N'2018-03-12T00:26:30.9375132' AS DateTime2), CAST(970.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (36, 49, N'Malmsteen Yngwie Johan', 2, 9, N'Fender Standard Stratocaster HSH PF Black Bundle W/ Fender Gig Bag, Stand, Tuner and Instrument Cable', CAST(N'2018-03-12T00:27:16.1277674' AS DateTime2), CAST(1374.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (37, 49, N'Malmsteen Yngwie Johan', 1, 27, N'2018 Dell Business Flagship Laptop Notebook 15.6" HD+ LED-Backlit Display Intel i5-7200U Processor 8GB DDR4 RAM 256GB HDD DVD-RW HDMI Webcam Bluetooth Windows 10 Pro-Black', CAST(N'2018-03-12T00:27:42.4703228' AS DateTime2), CAST(649.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (38, 43, N'Blackmore Richard Hugh', 1, 21, N'Yamaha L-Series Transacoustic Guitar - Concert Size, Vintage Natural', CAST(N'2018-03-12T00:28:03.4950070' AS DateTime2), CAST(999.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (39, 43, N'Blackmore Richard Hugh', 1, 9, N'Fender Standard Stratocaster HSH PF Black Bundle W/ Fender Gig Bag, Stand, Tuner and Instrument Cable', CAST(N'2018-03-12T00:28:27.7649176' AS DateTime2), CAST(687.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (40, 4, N'Google', 1, 29, N'HP 17.3" HD+ Notebook (2018 New), Intel Core i3-7100U Processor 2.4 GHz, 8GB Memory, 2TB Hard Drive, Optical Drive, HD Webcam, Backlit Keyboard, Windows 10 Home, Pale Mint', CAST(N'2018-03-12T00:29:05.7244941' AS DateTime2), CAST(627.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (41, 3, N'Microsoft', 1, 33, N'ibook G4 12" 1.2 GHZ, 512MB RAM, 30 GB HD, COMBO DRIVE, AIRPORT EXTREME', CAST(N'2018-03-12T00:29:23.8590321' AS DateTime2), CAST(1999.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (42, 2, N'dr. Watson', 1, 24, N'Apple MacBook Pro MGXA2LL/A 15-Inch Laptop with Retina Display (2.2 GHz Intel Core i7 Processor, 16 GB RAM, 256 GB HDD)', CAST(N'2018-03-12T00:29:41.3038670' AS DateTime2), CAST(1999.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (43, 1, N'Holmes Sherlock', 1, 22, N'Yamaha FS-TA Concert Size Transacoustic Guitar w/ Chorus and Reverb, Ruby Red', CAST(N'2018-03-12T00:30:06.6245770' AS DateTime2), CAST(599.00 AS Decimal(18, 2)))
INSERT [dbo].[Sales] ([Id], [ClientId], [ClientName], [Count], [ProductId], [ProductName], [SaleDate], [TotalCost]) VALUES (44, 1, N'Holmes Sherlock', 1, 27, N'2018 Dell Business Flagship Laptop Notebook 15.6" HD+ LED-Backlit Display Intel i5-7200U Processor 8GB DDR4 RAM 256GB HDD DVD-RW HDMI Webcam Bluetooth Windows 10 Pro-Black', CAST(N'2018-03-12T00:30:34.1158400' AS DateTime2), CAST(649.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Sales] OFF
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_ClientRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[ClientRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_ClientRoles_RoleId]
GO
ALTER TABLE [dbo].[LegalEntities]  WITH CHECK ADD  CONSTRAINT [FK_LegalEntities_Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LegalEntities] CHECK CONSTRAINT [FK_LegalEntities_Clients_ClientId]
GO
ALTER TABLE [dbo].[Persons]  WITH CHECK ADD  CONSTRAINT [FK_Persons_Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Persons] CHECK CONSTRAINT [FK_Persons_Clients_ClientId]
GO
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sales] CHECK CONSTRAINT [FK_Sales_Clients_ClientId]
GO
ALTER TABLE [dbo].[Sales]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sales] CHECK CONSTRAINT [FK_Sales_Products_ProductId]
GO

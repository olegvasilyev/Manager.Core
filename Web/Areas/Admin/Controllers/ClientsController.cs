﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.DTO.Clients;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Areas.Admin.Models.Clients;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "admin")]
    public class ClientsController : Controller
    {
        private readonly IClientsService _clients;
        private readonly IMapper _mapper;

        public ClientsController(IClientsService clients, IMapper mapper)
        {
            _clients = clients;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var roles = await _clients.GetClientRoles();
            return View(roles);
        }

        [HttpPost]
        public async Task<IActionResult> GetClients()
        {
            var parameters = AjaxParametersFactory.GetDataTableParameters(Request);
            var result = await _clients.GetClientsAsync(parameters);
            return Json(new
            {
                result.Draw,
                result.RecordsTotal,
                result.RecordsFiltered,
                result.Data,
                result.Error
            });
        }

        [HttpPost]
        public async Task<IActionResult> EditClient(ClientViewModel model)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var client = _mapper.Map<ClientViewModel, ClientDTO>(model);

                var res = await _clients.EditClientAsync(client);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Client not saved. Enter correct data";
            }

            return Json(new
            {
                result,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteClient(int id)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _clients.DeleteClientAsync(id);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetClient(int id)
        {
            var item = new ClientDTO();
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _clients.GetClientAsync(id);

                item = res.Data;
                result = res.IsSuccess;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                item,
                message
            });
        }
    }
}
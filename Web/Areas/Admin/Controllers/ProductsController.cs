﻿using ApplicationCore.DTO.Products;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Web.Areas.Admin.Models.Products;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private readonly IProductsService _products;
        private readonly IMapper _mapper;

        public ProductsController(IProductsService products, IMapper mapper)
        {
            _products = products;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetProducts()
        {
            var parameters = AjaxParametersFactory.GetDataTableParameters(Request);
            var result = await _products.GetProductsAsync(parameters);
            return Json(new
            {
                result.Draw,
                result.RecordsTotal,
                result.RecordsFiltered,
                result.Data,
                result.Error
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetProduct(int id)
        {
            var item = new ProductDTO();
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _products.GetProductAsync(id);

                item = res.Data;
                result = res.IsSuccess;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                item,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> EditProduct(ProductViewModel model)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var product = _mapper.Map<ProductViewModel, ProductDTO>(model);

                decimal price = 0;
                var style = NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowCurrencySymbol;
                var culture = CultureInfo.InvariantCulture;

                decimal.TryParse(model.Price, style, culture, out price);

                product.Price = price;

                var res = await _products.EditProductAsync(product);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Product was not saved. Enter correct data";
            }

            return Json(new
            {
                result,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> SearchProducts(string text)
        {
            var result = await _products.SearchProductsAsync(text);

            return Json(new
            {
                result
            });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _products.DeleteProductAsync(id);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                message
            });
        }
    }
}
﻿using ApplicationCore.DTO.Clients;
using ApplicationCore.DTO.Sales;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Areas.Admin.Models.Clients;
using Web.Areas.Admin.Models.Sales;

namespace Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SalesController : Controller
    {
        private readonly ISalesService _sales;
        private readonly IClientsService _clients;
        private readonly IMapper _mapper;

        public SalesController(ISalesService sales, IClientsService clients, IMapper mapper)
        {
            _sales = sales;
            _clients = clients;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index(int clientId)
        {
            var client = await _clients.GetClientAsync(clientId);

            var vm = _mapper.Map(client.Data, typeof(ClientDTO), typeof(ClientViewModel));

            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> EditSale(SaleViewModel model)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var sale = _mapper.Map<SaleViewModel, SaleDTO>(model);

                var res = await _sales.EditSaleAsync(sale);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Enter correct data";
            }

            return Json(new
            {
                result,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetSales()
        {
            var parameters = AjaxParametersFactory.GetDataTableParameters(Request);
            var result = await _sales.GetSalesAsync(parameters);
            return Json(new
            {
                result.Draw,
                result.RecordsTotal,
                result.RecordsFiltered,
                result.Data,
                result.Error
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetSale(int id)
        {
            var item = new SaleDTO();
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _sales.GetSaleAsync(id);

                item = res.Data;
                result = res.IsSuccess;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                item,
                message
            });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSale(int id)
        {
            var result = false;
            var message = "";

            if (ModelState.IsValid)
            {
                var res = await _sales.DeleteSaleAsync(id);

                result = res.Data;
                message = res.Message;
            }
            else
            {
                message = "Error operation";
            }

            return Json(new
            {
                result,
                message
            });
        }
    }
}
﻿using ApplicationCore.DTO.Clients;
using System.Collections.Generic;

namespace Web.Areas.Admin.Models.Clients
{
    public class ClientRolesViewModel
    {
        public List<ClientRoleDTO> Roles { get; set; }
    }
}

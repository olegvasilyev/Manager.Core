﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Admin.Models.Clients
{
    public class ClientViewModel
    {
        public int Id { get; set; } = 0;

        [Required]
        [MaxLength(32)]
        public string Country { get; set; } = "";

        [Required]
        [MaxLength(32)]
        public string City { get; set; } = "";

        [Required]
        [MaxLength(32)]
        public string Street { get; set; } = "";

        [MaxLength(16)]
        public string HomeNumber { get; set; } = null;

        public int RoleId { get; set; } = 0;

        [MaxLength(16)]
        public string FirstName { get; set; } = "";

        [MaxLength(16)]
        public string SurName { get; set; } = null;

        [MaxLength(16)]
        public string LastName { get; set; } = null;

        public DateTime? Birthday { get; set; } = null;

        [MaxLength(16)]
        public string Passport { get; set; } = "";

        [MaxLength(128)]
        public string Name { get; set; } = "";

        [MaxLength(16)]
        public string ITN { get; set; } = "";


        public string FullName { get; set; } = "";
    }
}

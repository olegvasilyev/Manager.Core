﻿using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Admin.Models.Products
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        public int Count { get; set; }

        [Required]
        public string Price { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Web.Areas.Admin.Models.Sales
{
    public class SaleViewModel
    {
        public int Id { get; set; } = 0;
        [Required]
        public int Count { get; set; } = 0;
        [Required]
        public int ClientId { get; set; } = 0;
        [Required]
        public int ProductId { get; set; } = 0;
        public string ClientName { get; set; } = "";
        public string ProductName { get; set; } = "";
    }
}

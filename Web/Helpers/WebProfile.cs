﻿using ApplicationCore.DTO.Clients;
using ApplicationCore.DTO.Products;
using ApplicationCore.DTO.Sales;
using AutoMapper;
using Web.Areas.Admin.Models.Clients;
using Web.Areas.Admin.Models.Products;
using Web.Areas.Admin.Models.Sales;

namespace Web.Helpers
{
    public class WebProfile : Profile
    {
        public WebProfile()
        {
            CreateMap<ClientDTO, ClientViewModel>();
            CreateMap<ClientViewModel, ClientDTO>();

            CreateMap<ProductViewModel, ProductDTO>().ForMember(x => x.Price, opt => opt.Ignore());
            CreateMap<ProductDTO, ProductViewModel>();

            CreateMap<SaleDTO, SaleViewModel>();
            CreateMap<SaleViewModel, SaleDTO>();
        }
    }
}

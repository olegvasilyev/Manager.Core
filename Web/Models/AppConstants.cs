﻿namespace Web.Models
{
    public static class AppConstants
    {
        public static string DefaultAdminController = "clients";
        public static string DefaultAdminMethod = "index";
        public static string AdminArea = "admin";

        public static string AdminRole = "admin";
        public static string UserRole = "user";
        public static string ClientPersonCode = "person";
        public static string ClientEntityCode = "entity";
    }
}

﻿using System;

namespace Web
{
    public class Settings
    {
        public string AppName { get; set; }
        public string Version { get; set; }
        public String Author { get; set; }
    }
}

﻿var crm = crm || {};

crm.clients = {
    init: function () {
        var $document = $(document);

        this.editModal = "#edit-client-modal";
        this.$editModal = $(this.editModal);

        this.editForm = "#edit-client-form";
        this.$editForm = $(this.editForm);

        this.clientsTable = $('#clients-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/admin/clients/getclients",
                "type": "POST",
            },
            columns: [
                {
                    data: 'id',
                    "visible": false,
                    "searchable": false
                },
                { data: 'name' },
                { data: 'roleName' },
                { data: 'address' },
                {
                    data: 'id',
                    "orderable": false,
                    className: "client-operations",
                    "width": "110px",
                    "render": function (data, type, row) {
                        var tmpl = $.templates("#client-operation-buttons");
                        var options = { id: row.id, name: row.name };
                        var html = tmpl.render(options);
                        return html;
                    },
                }
            ],
            "order": [[0, "desc"]],
            createdRow: function (row, data, dataIndex) {
                var $row = $(row);
                $row.find('td:eq(0)').attr('data-title', 'Id');
                $row.find('td:eq(1)').attr('data-title', 'Name');
                $row.find('td:eq(2)').attr('data-title', 'RoleName');
                $row.find('td:eq(3)').attr('data-title', 'Address');
                $row.find('td:eq(4)').attr('data-title', 'Operations');
            }
        });

        this.validator = this.$editForm.validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) { }
        });

        this.initHandlers();
    },

    initHandlers: function () {
        var $document = $(document);

        crm.clients.$editForm.on('submit', function (e) {

            if (crm.clients.$editForm.valid()) {
                var data = crm.clients.$editForm.serialize();

                $.ajax({
                    method: "POST",
                    url: "/admin/clients/editclient",
                    data: data
                })
                    .done(function (data) {
                        if (data.result) {
                            crm.clients.clientsTable.ajax.reload();
                        } else {
                            crm.clients.showAlert(data.message);
                        }
                    });

                crm.clients.$editModal.modal('hide');
            }

            return false;
        });

        $document.on('click', '#create-client-btn', function (e) {
            e.preventDefault();

            var code = $(crm.clients.editModal + " .custom-control-input:checked").data('code'),
                html = '';

            if (code === 'person') {
                var tmpl = $.templates("#person-fields");
                html = tmpl.render();
            }
            else {
                var tmpl = $.templates("#entity-fields");
                html = tmpl.render();
            }

            $(crm.clients.editModal + " #client-data").html('').append(html);
            $(crm.clients.editModal).modal('show');
        });

        $(crm.clients.editModal + " .custom-control-input").on("change", function () {
            var code = $(crm.clients.editModal + " .custom-control-input:checked").data('code'),
                html = '';

            if (code === 'person') {
                var tmpl = $.templates("#person-fields");
                html = tmpl.render();
            }
            else {
                var tmpl = $.templates("#entity-fields");
                html = tmpl.render();
            }

            $(crm.clients.editModal + " #client-data").hide(300, function () {
                $(crm.clients.editModal + " #client-data").html('').append(html).show(300);
            })
        });

        crm.clients.$editModal.on('hide.bs.modal', function (e) {
            crm.clients.validator.resetForm();
            crm.clients.$editForm[0].reset();
            $(crm.clients.editModal + " .custom-control-input").prop("disabled", false);
        })

        $document.on('click', '#clients-table .delete-client', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var clientID = $(element).data('client-id'),
                clientName = $(element).data('client-name');

            bootbox.confirm({
                message: "Delete " + clientName + "?",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "/admin/clients/deleteclient",
                            data: { id: clientID }
                        })
                            .done(function (data) {
                                if (data.result) {
                                    crm.clients.clientsTable.ajax.reload();
                                } else {
                                    crm.clients.showAlert(data.message);
                                }
                            });
                    }
                }
            })
        });

        $document.on('click', '#clients-table .edit-client', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var clientID = $(element).data('client-id'),
                client = {};

            $.ajax({
                method: "POST",
                url: "/admin/clients/getclient",
                data: { id: clientID }
            })
                .done(function (data) {
                    if (data.result) {
                        crm.clients.editClient(data.item);
                    } else {
                        crm.clients.showAlert(data.message);
                    }
                });
        });

        $document.on('click', '#clients-table .make-purchase', function (e) {
            e.preventDefault();

            var $element = $(e.currentTarget);

            var clientID = $element.data('client-id'),
                clientName = $element.data('client-name');

            $(crm.clients.purchaseModal + ' input[name="ClientId"]').val(clientID);
            $(crm.clients.purchaseModal + ' input[name="ClientName"]').val(clientName);

            crm.clients.$purchaseModal.modal('show');
        });
    },

    editClient: function (client) {

        $(crm.clients.editModal + ' #' + client.roleCode).prop('checked', true);
        $(crm.clients.editModal + " .custom-control-input").prop("disabled", true);

        $(crm.clients.editModal + ' input[name="Id"]').val(client.id);
        $(crm.clients.editModal + ' input[name="Country"]').val(client.country);
        $(crm.clients.editModal + ' input[name="City"]').val(client.city);
        $(crm.clients.editModal + ' input[name="Street"]').val(client.street);
        $(crm.clients.editModal + ' input[name="HomeNumber"]').val(client.homeNumber);

        if (client.roleCode === 'person') {
            var tmpl = $.templates("#person-fields");
            html = tmpl.render();
            $(crm.clients.editModal + " #client-data").html('').append(html)

            $(crm.clients.editModal + ' input[name="FirstName"]').val(client.firstName);
            $(crm.clients.editModal + ' input[name="SurName"]').val(client.surName);
            $(crm.clients.editModal + ' input[name="LastName"]').val(client.lastName);
            $(crm.clients.editModal + ' input[name="Passport"]').val(client.passport);
            $(crm.clients.editModal + ' input[name="Birthday"]').val(client.birthdayStr);
        }
        else {
            var tmpl = $.templates("#entity-fields");
            html = tmpl.render();
            $(crm.clients.editModal + " #client-data").html('').append(html)

            $(crm.clients.editModal + ' input[name="Name"]').val(client.name);
            $(crm.clients.editModal + ' input[name="ITN"]').val(client.itn);
        }

        crm.clients.$editModal.modal('show');
    },

    showAlert: function (message) {
        if (message === "") return;

        var alert = bootbox.dialog({ message: message, backdrop: false });
        setTimeout(function () { alert.modal('hide'); }, 3000);
    }
};
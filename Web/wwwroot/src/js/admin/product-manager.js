﻿var crm = crm || {};

crm.products = {
    init: function () {
        var $document = $(document);

        this.editModal = "#edit-product-modal";
        this.$editModal = $(this.editModal);

        this.editForm = "#edit-product-form";
        this.$editForm = $(this.editForm);

        this.productsTable = $('#products-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/admin/products/getproducts",
                "type": "POST",
            },
            columns: [
                {
                    data: 'id',
                    "visible": false,
                    "searchable": false
                },
                { data: 'name' },
                { data: 'price' },
                { data: 'count' },
                {
                    data: 'id',
                    "orderable": false,
                    className: "product-operations",
                    "width": "80px",
                    "render": function (data, type, row) {
                        var tmpl = $.templates("#product-operation-buttons");
                        var options = { id: row.id, name: row.name };
                        var html = tmpl.render(options);
                        return html;
                    },
                }
            ],
            "order": [[0, "desc"]],
            createdRow: function (row, data, dataIndex) {
                var $row = $(row);
                $row.find('td:eq(0)').attr('data-title', 'Id');
                $row.find('td:eq(1)').attr('data-title', 'Name');
                $row.find('td:eq(2)').attr('data-title', 'Price');
                $row.find('td:eq(3)').attr('data-title', 'Count');
                $row.find('td:eq(4)').attr('data-title', 'Operations');
            }
        });

        this.validator = this.$editForm.validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) { }
        });

        this.initHandlers();
    },

    initHandlers: function () {
        var $document = $(document);

        crm.products.$editForm.on('submit', function (e) {

            if (crm.products.$editForm.valid()) {
                var data = crm.products.$editForm.serialize();

                $.ajax({
                    method: "POST",
                    url: "/admin/products/editproduct",
                    data: data
                })
                    .done(function (data) {
                        if (data.result) {
                            crm.products.productsTable.ajax.reload();
                        } else {
                            crm.products.showAlert(data.message);
                        }
                    });
                crm.products.$editModal.modal('hide');
            }

            return false;
        });

        $document.on('click', '#create-product-btn', function (e) {
            e.preventDefault();

            $(crm.products.editModal).modal('show');
        });

        crm.products.$editModal.on('hide.bs.modal', function (e) {
            crm.products.validator.resetForm();
            crm.products.$editForm[0].reset();
        })

        $document.on('click', '#products-table .delete-product', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var productID = $(element).data('product-id'),
                productName = $(element).data('product-name');

            bootbox.confirm({
                message: "Delete " + productName + "?",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "/admin/products/deleteproduct",
                            data: { id: productID }
                        })
                            .done(function (data) {
                                if (data.result) {
                                    crm.products.productsTable.ajax.reload();
                                } else {
                                    crm.products.showAlert(data.message);
                                }
                            });
                    }
                }
            })
        });

        $document.on('click', '#products-table .edit-product', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var productID = $(element).data('product-id'),
                product = {};

            $.ajax({
                method: "POST",
                url: "/admin/products/getproduct",
                data: { id: productID }
            })
                .done(function (data) {
                    if (data.result)
                        crm.products.editProduct(data.item);
                    crm.products.showAlert(data.message);
                });
        });
    },

    editProduct: function (product) {

        $(crm.products.editModal + ' input[name="Id"]').val(product.id);
        $(crm.products.editModal + ' textarea[name="Name"]').val(product.name);
        $(crm.products.editModal + ' input[name="Price"]').val(product.price);
        $(crm.products.editModal + ' input[name="Count"]').val(product.count);

        crm.products.$editModal.modal('show');
    },

    showAlert: function (message) {
        if (message === "") return;

        var alert = bootbox.dialog({ message: message, backdrop: false });
        setTimeout(function () { alert.modal('hide'); }, 3000);
    }
};
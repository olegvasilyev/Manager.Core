﻿var crm = crm || {};

crm.sales = {
    init: function (clientId) {
        var $document = $(document);

        this.clientId = clientId;

        this.purchaseModal = "#make-purchase-modal";
        this.$purchaseModal = $(this.purchaseModal);

        this.purchaseForm = "#make-purchase-form";
        this.$purchaseForm = $(this.purchaseForm);

        this.editModal = "#edit-sale-modal";
        this.$editModal = $(this.editModal);

        this.editForm = "#edit-sale-form";
        this.$editForm = $(this.editForm);

        this.salesTable = $('#sales-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/admin/sales/getsales",
                "type": "POST",
                "data": function (d) {
                    return $.extend({}, d, {
                        "itemId": crm.sales.clientId,
                        "extra_search": ""
                    })
                }
            },
            columns: [
                {
                    data: 'id',
                    "visible": false,
                    "searchable": false
                },
                {
                    data: 'clientName',
                    "visible": false,
                    "searchable": false
                },
                { data: 'productName' },
                { data: 'saleDateStr' },
                { data: 'count' },
                { data: 'totalCost' },
                {
                    data: 'id',
                    "orderable": false,
                    className: "sale-operations",
                    "width": "80px",
                    "render": function (data, type, row) {
                        var tmpl = $.templates("#sale-operation-buttons");
                        var options = { id: row.id, clientName: row.clientName, productName: row.productName };
                        var html = tmpl.render(options);
                        return html;
                    },
                }
            ],
            "order": [[0, "desc"]],
            createdRow: function (row, data, dataIndex) {
                var $row = $(row);
                $row.find('td:eq(0)').attr('data-title', 'Id');
                $row.find('td:eq(1)').attr('data-title', 'ClientName');
                $row.find('td:eq(2)').attr('data-title', 'ProductName');
                $row.find('td:eq(3)').attr('data-title', 'SaleDate');
                $row.find('td:eq(4)').attr('data-title', 'Count');
                $row.find('td:eq(5)').attr('data-title', 'TotalCost');
                $row.find('td:eq(6)').attr('data-title', 'Operations');
            }
        });

        this.purchaseValidator = this.$purchaseForm.validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) { }
        });

        this.initHandlers();
    },

    initHandlers: function () {
        var $document = $(document);

        $document.on('click', '#sales-table .delete-sale', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var saleID = $(element).data('sale-id'),
                productName = $(element).data('product-name');

            bootbox.confirm({
                message: "Delete " + productName + "?",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "/admin/sales/deletesale",
                            data: { id: saleID }
                        })
                            .done(function (data) {
                                if (data.result) {
                                    crm.sales.salesTable.ajax.reload();
                                } else {
                                    crm.sales.showAlert(data.message);
                                }
                            });
                    }
                }
            })
        });

        crm.sales.$purchaseModal.on('hide.bs.modal', function (e) {
            crm.sales.purchaseValidator.resetForm();
            crm.sales.$purchaseForm[0].reset();
        });

        crm.sales.$editModal.on('hide.bs.modal', function (e) {
            crm.sales.$editForm[0].reset();
        });

        crm.sales.$purchaseForm.on('submit', function (e) {

            if (crm.sales.$purchaseForm.valid()) {
                var data = crm.sales.$purchaseForm.serialize();

                $.ajax({
                    method: "POST",
                    url: "/admin/sales/editsale",
                    data: data
                })
                    .done(function (data) {
                        if (data.result) {
                            crm.sales.salesTable.ajax.reload();
                        } else {
                            crm.sales.showAlert(data.message);
                        }
                    });
                crm.sales.$purchaseModal.modal('hide');
            }

            return false;
        });

        crm.sales.$editForm.on('submit', function (e) {

            if (crm.sales.$editForm.valid()) {
                var data = crm.sales.$editForm.serialize();

                $.ajax({
                    method: "POST",
                    url: "/admin/sales/editsale",
                    data: data
                })
                    .done(function (data) {
                        if (data.result) {
                            crm.sales.salesTable.ajax.reload();
                        } else {
                            crm.sales.showAlert(data.message);
                        }
                    });
                crm.sales.$editModal.modal('hide');
            }

            return false;
        });

        $document.on('click', '#sales-table .edit-sale', function (e) {
            e.preventDefault();

            var element = e.currentTarget;
            var id = $(element).data('sale-id');

            $.ajax({
                method: "POST",
                url: "/admin/sales/getsale",
                data: { id: id }
            })
                .done(function (data) {
                    if (data.result) {
                        crm.sales.editSale(data.item);
                    }
                    else {
                        crm.sales.showAlert(data.message);
                    }
                });
        });

        $(crm.sales.purchaseModal + ' input[name="ProductName"]').autocomplete({
            delay: 500,
            minLength: 2,
            source: function (request, response) {
                $.ajax({
                    method: "POST",
                    url: "/admin/products/SearchProducts",
                    data: { text: request.term }
                }).done(function (data) {
                    var list = [];

                    for (var i in data.result.data) {
                        var item = data.result.data[i];

                        list.push({
                            label: item.name + '...$' + item.price + '...' + item.count + 'pcs',
                            data: item
                        });
                    }
                    response(list);
                });
            },
            select: function (event, ui) {
                var count = $(crm.sales.purchaseModal + ' input[name="Count"]').val();
                var price = count * ui.item.data.price;

                $(crm.sales.purchaseModal + ' input[name="TotalCost"]').val(price);
                $(crm.sales.purchaseModal + ' input[name="ProductId"]').val(ui.item.data.id);
                $(crm.sales.purchaseModal + ' input[name="Price"]').val(price);
            }
        });

        $document.on('change', crm.sales.purchaseModal + ' input[name="Count"]', function (e) {
            e.preventDefault();

            var count = $(crm.sales.purchaseModal + ' input[name="Count"]').val();
            var price = $(crm.sales.purchaseModal + ' input[name="Price"]').val();

            if (count <= 0 || price === 0)
                return;

            var result = price * count;

            $(crm.sales.purchaseModal + ' input[name="TotalCost"]').val(result);
        });

        $document.on('click', crm.sales.purchaseModal + ' #clear-selection', function (e) {
            e.preventDefault();

            $(crm.sales.purchaseModal + ' input[name="ProductName"]').val('');
            $(crm.sales.purchaseModal + ' input[name="ProductId"]').val('');
            $(crm.sales.purchaseModal + ' input[name="Price"]').val('');
            $(crm.sales.purchaseModal + ' input[name="TotalCost"]').val('');
            $(crm.sales.purchaseModal + ' input[name="Count"]').val(1);
        });

        $document.on('change', crm.sales.editModal + ' input[name="Count"]', function (e) {
            e.preventDefault();

            var count = $(e.currentTarget).val();
            var price = $(crm.sales.editModal + ' input[name="Price"]').val();

            if (count <= 0 || price === 0)
                return;

            var result = price * count;

            $(crm.sales.editModal + ' input[name="TotalCost"]').val(result);
        });
    },

    editSale: function (sale) {

        $(crm.sales.editModal + ' input[name="Id"]').val(sale.id);
        $(crm.sales.editModal + ' input[name="ProductId"]').val(sale.productId);
        $(crm.sales.editModal + ' input[name="Price"]').val(sale.productPrice);
        $(crm.sales.editModal + ' input[name="ProductName"]').val(sale.productName);
        $(crm.sales.editModal + ' input[name="Count"]').val(sale.count);
        $(crm.sales.editModal + ' input[name="ProductCount"]').val(sale.productCount);
        $(crm.sales.editModal + ' input[name="TotalCost"]').val(sale.totalCost);

        crm.sales.$editModal.modal('show');
    },

    showAlert: function (message) {
        if (message === "") return;

        var alert = bootbox.dialog({ message: message, backdrop: false });
        setTimeout(function () { alert.modal('hide'); }, 3000);
    }
};
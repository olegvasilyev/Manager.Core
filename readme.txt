ASP.NET Core 2.0 project Manager

Decription. Manager is a simple pure ASP.NET Core 2.0 project, that is representing
    simple CRM functionality (clients, products, sales/purchases). It can be 
    used in Linus with PostgreSQL as well as in Windows.
    There are two roles predefined, user and admin and one user created.

Development environment:
1.  Visual Studio 2017.
2.  MSSQL Server
3.  .NET Core 2.0.0 SDK installed. 
    Check for details: https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?tabs=aspnetcore2x

How to start:
1.  Take in SQL folder script to create database, or restore database from backupDB.bak
    in your MSSQL server.
2.  Cope connection string to database you have created above and replace existing one
    in appsettings.json file.
3.  Use login = "admin@admin.com",  password = "_Aa123456" to login and goto cabinet
    or you can register yourself as a new user. A role "admin" will be assignet and
    you can work.

Add:
-   The architectrure is N-Layer (three layers) and well descibed in
    https://blogs.msdn.microsoft.com/dotnet/2017/08/09/web-apps-aspnetcore-architecture-guidance/
-   gulpfile disignet to make two custom scripts for main view pages and cabinet
-   Additional frameworks and tools: jQuery 3.2.1, Bootstra 4, FontAwesome 5.0.3,
    sbadmin, jsRender and some small libraries and components
